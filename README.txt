Standard Batch Starter Kit README.txt

The Standard Batch Starter Kit refactors the Drupal Standard installation 
profile to install as a batch operation. Its main purpose is to get you started 
on your own custom profile using batch operations and non-interactive installs.
The only feature it provides in addition to standard Drupal features is the 
ability to change the default and admin themes by using a setting in the 
install-batch.php file. It also makes a few minor code changes to the standard
installation profile. 

Installation: Installs just like any Drupal installation profile. Download the
project files and unzip into DRUPAL_ROOT/profiles/standard_batch directory.
Important: This location is hardcoded and cannot be changed. Set up your 
database and settings.php file as you would for any Drupal installation, then
navigate to your_site.com/install.php and choose Standard Batch Starter Kit as 
the profile to install.

Non-interactive installation: Copy or link the install-batch.php file to the 
root directory of your Drupal installation. Open the file and provide values for
the settings array. The keys in the settings array correspond to fields that you
would complete manually during an installation from the web browser. From the 
command line, type: php install-batch.php.

Multisite installation: For a multi-site installation you need to add the 
'server' key to the settings array, something like this:
 <?php
   'server' => array(
     'url' => 'http://yoursite.example.com/install.php',
   ),
 ?>
Anything you put in 'server' gets passed by the installer to 
drupal_override_server_variables() where it's used to set up the $_SERVER value 
as if the request came from a web browser.

Pitfalls to avoid: PHP runs under your permissions, so make sure you have write
access to the necessary files and directories, or run install-batch.php as the 
super-user. It is best to set up your database settings in settings.php as you
would normally do. If you try to use the forms key in the settings array to do 
this, the installer will try to connect to the database using your local user 
credentials.

Interactive installation using install-batch.php: Change the Interactive setting
to TRUE and navigate to example.com/install-batch.php. The installation will use 
the parameters you have set in the settings array, but you will have to fill out
the install forms manually.
