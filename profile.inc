<?php

/**
 * @file
 * Container for batch operation callback functions
 */

/**
 * Install miscellaneous configuration items
 */
function standard_batch_install_config(&$context) {
  // Default configurations set here for convenience
  // Allow visitor account creation, but with administrative approval.
  variable_set('user_register', USER_REGISTER_VISITORS_ADMINISTRATIVE_APPROVAL);

  // Enable user picture support and set the default to a square thumbnail option.
  variable_set('user_pictures', '1');
  variable_set('user_picture_dimensions', '1024x1024');
  variable_set('user_picture_file_size', '800');
  variable_set('user_picture_style', 'thumbnail');

  // Create a Home link in the main menu.
  $item = array(
    'link_title' => st('Home'),
    'link_path' => '<front>',
    'menu_name' => 'main-menu',
  );
  menu_link_save($item);
  //omits the call to menu_rebuild, because menus are rebuilt when the installation finishes.

  // Site specific configuration begins here

  //Set the message to display in the browser */
  $context['message'] = 'Installed default configuration';
}










