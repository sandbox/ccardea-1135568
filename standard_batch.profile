<?php

/**
 * @file
 * Provides installation tasks and callback functions 
 */

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Allows the profile to alter the site configuration form.
 */
function standard_batch_form_install_configure_form_alter(&$form, $form_state) {
  // Pre-populate the site name with the server name.
  $form['site_information']['site_name']['#default_value'] = $_SERVER['SERVER_NAME'];
}

/**
 * Implements hook_install_tasks
 *
 * @return
 *   An associative array containing the install task definitions
 */
function standard_batch_install_tasks() {
  $tasks = array(
    'standard_batch_install_drupal' => array(
      'display_name' => st('Install Drupal Defaults'),
      'display' => TRUE,
      'type' => 'batch',
      'run' => INSTALL_TASK_RUN_IF_NOT_COMPLETED,
    ),
    'standard_batch_install_custom' => array(
      'display_name' => st('Install Custom'),
      'display' => TRUE,
      'type' => 'batch',
      'run' => INSTALL_TASK_RUN_IF_NOT_COMPLETED,
    ),
  );
  return $tasks;
}

/**
 * Install Drupal defaults
 *
 * Sets up batch operations for a standard Drupal install
 *
 * @param $install_state:
 *   An array containing the settings from the install-batch.php file
 *
 * @return
 *   The batch settings array
 *
 * @see standard_batch_install_tasks().
 * @see http://api.drupal.org/api/drupal/includes--form.inc/function/batch_set/7
 */
function standard_batch_install_drupal(&$install_state) {
  // Get theme defaults 
  $args = standard_batch_theme_defaults($install_state);
  // Set up operations
  $operations = array(
    array('standard_batch_install_themes', $args),
    array('standard_batch_install_blocks', $args),
    array('standard_batch_install_filters', array()),
    array('standard_batch_install_page', array()),
    array('standard_batch_install_article', array()),
    array('standard_batch_install_rdf', array()),
    array('standard_batch_install_access_control', array()),
  );
  //create the batch array
  $batch = array(
    'operations' => $operations,
  );
  return $batch;
}

/**
 * Install task callback
 *
 * Sets up batch operations to install custom features
 *
 * @param $install_state
 * An array containing the settings from the install-batch.php file
 *
 * @return
 * The batch settings array
 *
 * @todo change the path to file when drupal_get_path is fixed.
 *
 * @see standard_batch_install_tasks().
 */
function standard_batch_install_custom(&$install_state) {
  // drupal_get_path will not return a path to a profile, so the path is hardcoded
   $batch = array(
    'operations' => array(
      array('standard_batch_install_config', array()),
    ),
    'file' => 'profiles/standard_batch/profile.inc',
  );
  return $batch;
}

/**
 * Install default theme and admin theme
 *
 * @param $admin
 *   The administration theme
 * @param $default
 *   The default theme
 * @param $context
 *   Used by batch system to control batch operations
 */
function standard_batch_install_themes($admin, $default, &$context) {
  // Enable the admin theme.
  $themes = array();
  $themes['admin_theme'] = $admin;
  variable_set('admin_theme', $admin);
  variable_set('node_admin_theme', '1');

  //enable the default theme if different from bartik
  $system_default = variable_get('theme_default');
  if ($default != $system_default) {
    $themes['default_theme'] = $default;
    variable_set('theme_default', $default);
    // make sure that the old default theme is not the admin theme before disabling it.
    if ($admin != $system_default) {
      theme_disable(array($system_default));
    }
  }

  $context['message'] = 'Installed themes';
  theme_enable($themes);
}

/**
 *  Enable default system blocks
 *
 * @param $admin_theme
 *   The administration theme
 * @param $default_theme
 *   The default theme
 * @param $context
 *   Used by batch system to control batch operations
 */
function standard_batch_install_blocks($admin_theme, $default_theme, &$context) {
  $blocks = array(
  // default theme blocks
    array(
      'module' => 'system',
      'delta' => 'main',
      'theme' => $default_theme,
      'status' => 1,
      'weight' => 0,
      'region' => 'content',
      'pages' => '',
      'cache' => -1,
    ),
    array(
      'module' => 'search',
      'delta' => 'form',
      'theme' => $default_theme,
      'status' => 1,
      'weight' => -1,
      'region' => 'sidebar_first',
      'pages' => '',
      'cache' => -1,
    ),
    array(
      'module' => 'user',
      'delta' => 'login',
      'theme' => $default_theme,
      'status' => 1,
      'weight' => 0,
      'region' => 'sidebar_first',
      'pages' => '',
      'cache' => -1,
    ),
    array(
      'module' => 'system',
      'delta' => 'navigation',
      'theme' => $default_theme,
      'status' => 1,
      'weight' => 0,
      'region' => 'sidebar_first',
      'pages' => '',
      'cache' => -1,
    ),
    array(
      'module' => 'system',
      'delta' => 'powered-by',
      'theme' => $default_theme,
      'status' => 1,
      'weight' => 10,
      'region' => 'footer',
      'pages' => '',
      'cache' => -1,
    ),
    array(
      'module' => 'system',
      'delta' => 'help',
      'theme' => $default_theme,
      'status' => 1,
      'weight' => 0,
      'region' => 'help',
      'pages' => '',
      'cache' => -1,
    ),
    array(
      'module' => 'system',
      'delta' => 'main',
      'theme' => $admin_theme,
      'status' => 1,
      'weight' => 0,
      'region' => 'content',
      'pages' => '',
      'cache' => -1,
    ),
    array(
      'module' => 'system',
      'delta' => 'help',
      'theme' => $admin_theme,
      'status' => 1,
      'weight' => 0,
      'region' => 'help',
      'pages' => '',
      'cache' => -1,
    ),
    array(
      'module' => 'user',
      'delta' => 'login',
      'theme' => $admin_theme,
      'status' => 1,
      'weight' => 10,
      'region' => 'content',
      'pages' => '',
      'cache' => -1,
    ),
    array(
      'module' => 'node',
      'delta' => 'recent',
      'theme' => $admin_theme,
      'status' => 1,
      'weight' => 10,
      'region' => 'dashboard_main',
      'pages' => '',
      'cache' => -1,
    ),
    array(
      'module' => 'user',
      'delta' => 'new',
      'theme' => $admin_theme,
      'status' => 1,
      'weight' => 0,
      'region' => 'dashboard_sidebar',
      'pages' => '',
      'cache' => -1,
    ),
    array(
      'module' => 'search',
      'delta' => 'form',
      'theme' => $admin_theme,
      'status' => 1,
      'weight' => -10,
      'region' => 'dashboard_sidebar',
      'pages' => '',
      'cache' => -1,
    ),
  );

  //update the database //
  foreach ($blocks as $block) {
    db_update('block')
      ->fields(array(
        'status' => $block['status'],
        'weight' => $block['weight'],
        'region' => $block['region'],
        'pages' => $block['pages'],
        'cache' => $block['cache'],
      ))
      ->condition('module', $block['module'])
      ->condition('delta', $block['delta'])
      ->condition('theme', $block['theme'])
      ->execute();
  }
  //Set the message to appear in the browser
  $context['message'] = 'Enabled default blocks';
}

/**
 * Install default text filters 
 *
 * @param $context
 *   Used by batch system to control batch operations
 */
function standard_batch_install_filters(&$context) {
  $filtered_html_format = array(
    'format' => 'filtered_html',
    'name' => 'Filtered HTML',
    'weight' => 0,
    'filters' => array(
      // URL filter.
      'filter_url' => array(
        'weight' => 0,
        'status' => 1,
      ),
      // HTML filter.
      'filter_html' => array(
        'weight' => 1,
        'status' => 1,
      ),
      // Line break filter.
      'filter_autop' => array(
        'weight' => 2,
        'status' => 1,
      ),
      // HTML corrector filter.
      'filter_htmlcorrector' => array(
        'weight' => 10,
        'status' => 1,
      ),
    ),
  );
  $filtered_html_format = (object) $filtered_html_format;
  filter_format_save($filtered_html_format);

  $full_html_format = array(
    'format' => 'full_html',
    'name' => 'Full HTML',
    'weight' => 1,
    'filters' => array(
      // URL filter.
      'filter_url' => array(
        'weight' => 0,
        'status' => 1,
      ),
      // Line break filter.
      'filter_autop' => array(
        'weight' => 1,
        'status' => 1,
      ),
      // HTML corrector filter.
      'filter_htmlcorrector' => array(
        'weight' => 10,
        'status' => 1,
      ),
    ),
  );
  $full_html_format = (object) $full_html_format;
  filter_format_save($full_html_format);

  // Save the filtered permission name for use in setting access control
  variable_set('filtered_html_permission', filter_permission_name($filtered_html_format));

  // Set the message to appear in the browser
  $context['message'] = 'Installed Filter Formats';
}

/**
 * Install default access control
 *
 * Enables default permissions for system roles
 * Creates the default administrator role and assigns all permissions
 *
 * @param $context
 *   Used by batch system to control batch operations
 */
function standard_batch_install_access_control(&$context) {
  // Enable default permissions for system roles.
  $filtered_html = variable_get('filtered_html_permission');
  user_role_grant_permissions(DRUPAL_ANONYMOUS_RID, array('access content', 'access comments', $filtered_html));
  user_role_grant_permissions(DRUPAL_AUTHENTICATED_RID, array('access content', 'access comments', 'post comments', 'skip comment approval', $filtered_html));

  // Create a default role for site administrators, with all available permissions assigned.
  $admin_role = new stdClass();
  $admin_role->name = 'administrator';
  $admin_role->weight = 2;
  user_role_save($admin_role);
  user_role_grant_permissions($admin_role->rid, array_keys(module_invoke_all('permission')));
  // Set this as the administrator role.//
  variable_set('user_admin_role', $admin_role->rid);

  // Assign user 1 the "administrator" role.
  db_insert('users_roles')
    ->fields(array('uid' => 1, 'rid' => $admin_role->rid))
    ->execute();

  /*Set the message to display in the browser */
  $context['message'] = 'Installed access control defaults';
}

/**
 * Install Page bundle
 *
 * @param $context
 *   Used by batch system to control batch operations
 */
function standard_batch_install_page(&$context) {
  // Insert default pre-defined node types into the database. For a complete
  // list of available node type attributes, refer to the node type API
  // documentation at: http://api.drupal.org/api/HEAD/function/hook_node_info.
  $types = array(
    array(
      'type' => 'page',
      'name' => st('Basic page'),
      'base' => 'node_content',
      'description' => st("Use <em>basic pages</em> for your static content, such as an 'About us' page."),
      'custom' => 1,
      'modified' => 1,
      'locked' => 0,
    ),
  );

  foreach ($types as $type) {
    $type = node_type_set_defaults($type);
    node_type_save($type);
    node_add_body_field($type);
  }

  // Default "Basic page" to not be promoted and have comments disabled.
  variable_set('node_options_page', array('status'));
  variable_set('comment_page', COMMENT_NODE_HIDDEN);

  // Don't display date and author information for "Basic page" nodes by default.
  variable_set('node_submitted_page', FALSE);

   $context['message'] = 'Installed Page bundle';
}


/**
 * Install Article bundle
 *
 * @param $context
 *   Used by batch system to control batch operations
 */
function standard_batch_install_article(&$context) {
  //Create Article node type
  $type = array(
    'type' => 'article',
    'name' => st('Article'),
    'base' => 'node_content',
    'description' => st('Use <em>articles</em> for time-sensitive content like news, press releases or blog posts.'),
    'custom' => 1,
    'modified' => 1,
    'locked' => 0,
  );
  $type = node_type_set_defaults($type);
  node_type_save($type);
  $body = node_add_body_field($type);
  // define default weights for the body field
  $body['widget']['weight'] = 0;
  $body['display']['default']['weight'] = 0;
  $body['display']['teaser']['weight'] = 0;
  field_update_instance($body);

  // Create field instances for the Article bundle
  standard_batch_install_taxonomy();
  standard_batch_install_image_field();

  $context['message'] = 'Installed Article bundle';
}

/**
 * Install default rdf mappings
 *
 * @param $context
 *   Used by batch system to control batch operations
 */
function standard_batch_install_rdf(&$context) {
  // Insert default pre-defined RDF mapping into the database.
  $rdf_mappings = array(
    array(
      'type' => 'node',
      'bundle' => 'page',
      'mapping' => array(
        'rdftype' => array('foaf:Document'),
      ),
    ),
    array(
      'type' => 'node',
      'bundle' => 'article',
      'mapping' => array(
        'field_image' => array(
          'predicates' => array('og:image', 'rdfs:seeAlso'),
          'type' => 'rel',
        ),
        'field_tags' => array(
          'predicates' => array('dc:subject'),
          'type' => 'rel',
        ),
      ),
    ),
  );
  foreach ($rdf_mappings as $rdf_mapping) {
    rdf_mapping_save($rdf_mapping);
  }
  $context['message'] = 'Installed default rdf mappings';
}

/**
 * Helper function for install_theme operation
 *
 * @param $install_state
 *   Contains the settings array from install-batch.php 
 *
 * @return
 *   An array containing the names of the default theme and admin theme
 */
function standard_batch_theme_defaults($install_state) {

  // set default themes to drupal7 defaults
  $admin = 'seven';
  $default = 'bartik';

  // if specified, use default themes from settings file instead
  $search = $install_state['parameters'];
  if (array_key_exists('admin_theme', $search)) {
    $admin = $search['admin_theme'];
  }
  if (array_key_exists('theme_default', $search)) {
    $default = $search['theme_default'];
  }

  // return an array of arguments to be passed to the operations callback 
  $args = array($admin, $default);
  return $args;
}

/**
 * Install default taxonomy
 *
 * Helper function installs the default tags vocabulary creates a taxonomy term 
 * reference field and the field instance for the article bundle
 */
function standard_batch_install_taxonomy() {
  // Create a default vocabulary named "Tags", enabled for the 'article' content type.
  $description = st('Use tags to group articles on similar topics into categories.');
  $help = st('Enter a comma-separated list of words to describe your content.');
  $vocabulary = (object) array(
    'name' => 'Tags',
    'description' => $description,
    'machine_name' => 'tags',
    'help' => $help,

  );
  taxonomy_vocabulary_save($vocabulary);

  //Create the taxonomy term reference field for the tags vocabulary
  $field = array(
    'field_name' => 'field_' . $vocabulary->machine_name,
    'type' => 'taxonomy_term_reference',
    // Set cardinality to unlimited for tagging.
    'cardinality' => FIELD_CARDINALITY_UNLIMITED,
    'settings' => array(
      'allowed_values' => array(
        array(
          'vocabulary' => $vocabulary->machine_name,
          'parent' => 0,
        ),
      ),
    ),
  );
  field_create_field($field);

  $instance = array(
    'field_name' => 'field_' . $vocabulary->machine_name,
    'entity_type' => 'node',
    'label' => $vocabulary->name,
    'bundle' => 'article',
    'description' => $vocabulary->help,
    'widget' => array(
      'type' => 'taxonomy_autocomplete',
      'weight' => -4,
    ),
    'display' => array(
      'default' => array(
        'type' => 'taxonomy_term_reference_link',
        'weight' => 10,
      ),
      'teaser' => array(
        'type' => 'taxonomy_term_reference_link',
        'weight' => 10,
      ),
    ),
  );
  field_create_instance($instance);
}

/**
 * Install image field
 *
 * Helper function to install the default image field and create a field 
 * instance for the article bundle
 */
function standard_batch_install_image_field() {
  // Installs the default image field
  $field = array(
    'field_name' => 'field_image',
    'type' => 'image',
    'cardinality' => 1,
    'translatable' => TRUE,
    'locked' => FALSE,
    'indexes' => array('fid' => array('fid')),
    'settings' => array(
      'uri_scheme' => 'public',
      'default_image' => FALSE,
    ),
    'storage' => array(
      'type' => 'field_sql_storage',
      'settings' => array(),
    ),
  );
  field_create_field($field);


  // Create the field instance
  $instance = array(
    'field_name' => 'field_image',
    'entity_type' => 'node',
    'label' => 'Image',
    'bundle' => 'article',
    'description' => st('Upload an image to go with this article.'),
    'required' => FALSE,

    'settings' => array(
      'file_directory' => 'field/image',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '1M',
      'max_resolution' => '1024',
      'min_resolution' => '',
      'alt_field' => TRUE,
      'title_field' => '',
    ),
  
    'widget' => array(
      'type' => 'image_image',
      'settings' => array(
        'progress_indicator' => 'throbber',
        'preview_image_style' => 'thumbnail',
      ),
      'weight' => -1,
    ),

    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'type' => 'image',
        'settings' => array('image_style' => 'large', 'image_link' => ''),
        'weight' => -1,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'type' => 'image',
        'settings' => array('image_style' => 'medium', 'image_link' => 'content'),
        'weight' => -1,
      ),
    ),
  );
 
 field_create_instance($instance);
}
